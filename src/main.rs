#![deny(clippy::all, clippy::cargo, unsafe_code)]

// see: https://docs.rs/zbus/2.0.0/zbus/struct.Connection.html

mod config;
mod flatpak;
mod hook;
mod portal;
mod process;
mod sessions;

use futures::stream::TryStreamExt;
use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};
use zbus::{
    fdo::DBusProxy,
    names::{BusName, UniqueName},
    Connection, Message, MessageField, MessageFieldCode, MessageStream,
};
use zvariant::{DeserializeDict, ObjectPath, TypeDict};

use crate::{
    config::{read_config_file, Config},
    portal::{
        is_create_session, is_portal_interface, is_request_response, is_session_close, Interface,
    },
    sessions::{session_closed, session_created, session_requested, Session},
};

#[tokio::main]
async fn main() -> zbus::Result<()> {
    let cfg = Config::from(read_config_file().await);
    eprintln!("{} configuration: {:?}", env!("CARGO_PKG_NAME"), &cfg);

    let connection = Connection::session().await?;

    connection
        .call_method(
            Some("org.freedesktop.DBus"),
            "/org/freedesktop/DBus",
            Some("org.freedesktop.DBus.Monitoring"),
            "BecomeMonitor",
            &(MATCH_RULES, 0u32),
        )
        .await?;

    let sessions = Arc::new(Mutex::new(HashMap::new()));

    // separate connection for querying message/sender metadata
    let metadata_connection = Connection::session().await?;
    let metadata_proxy = DBusProxy::new(&metadata_connection).await?;

    let mut stream = MessageStream::from(connection);
    while let Some(msg) = stream.try_next().await? {
        if !is_portal_interface(&msg) {
            continue;
        }
        if let Err(e) = handle_portal_message(&msg, &cfg, sessions.clone(), &metadata_proxy).await {
            eprintln!("processing dbus message: {:?}\n  error: {:?}", &msg, e);
        }
    }

    eprintln!("{} exiting ...", env!("CARGO_PKG_NAME"));
    Ok(())
}

// see: https://dbus.freedesktop.org/doc/dbus-specification.html#message-bus-routing-match-rules
const MATCH_RULES: &[&str] = &["path_namespace='/org/freedesktop/portal'"];

async fn handle_portal_message(
    msg: &Message,
    cfg: &Config,
    sessions: Arc<Mutex<HashMap<String, Session>>>,
    metadata_proxy: &DBusProxy<'_>,
) -> zbus::Result<()> {
    if is_create_session(msg) {
        if let (
            Ok(interface),
            Some(sender),
            Ok(CreateSessionOptions {
                session_handle_token: Some(session_handle_token),
            }),
        ) = (
            Interface::try_from(msg.interface()),
            get_sender(msg),
            msg.body::<CreateSessionOptions>(),
        ) {
            let process_id = metadata_proxy
                .get_connection_unix_process_id(BusName::Unique(sender.clone()))
                .await?;

            let session_path = generate_session_path(&sender, &session_handle_token);

            session_requested(session_path, interface, process_id, cfg, sessions.clone()).await;
        }
    } else if is_request_response(msg) {
        if let Ok((
            code,
            CreateSessionResults {
                session_handle: Some(session_handle),
            },
        )) = msg.body::<(u32, CreateSessionResults)>()
        {
            if code == 0 {
                session_created(session_handle, cfg, sessions.clone()).await;
            } else {
                session_closed(session_handle, cfg, sessions.clone()).await;
            }
        }
    } else if is_session_close(msg) {
        if let Some(path) = get_path(msg) {
            session_closed(String::from(path.as_str()), cfg, sessions.clone()).await;
        }
    }

    Ok(())
}

fn generate_session_path(sender: &UniqueName, session_handle: &str) -> String {
    let sender_path = sender.replacen(':', "", 1).replace('.', "_");
    format!(
        "/org/freedesktop/portal/desktop/session/{}/{}",
        sender_path, session_handle
    )
}

fn get_path(msg: &Message) -> Option<ObjectPath> {
    match msg.fields() {
        Ok(fields) => {
            if let Some(MessageField::Path(path)) = fields.get_field(MessageFieldCode::Path) {
                Some(path.clone())
            } else {
                None
            }
        }
        Err(_) => None,
    }
}

fn get_sender(msg: &Message) -> Option<UniqueName> {
    if let Ok(fields) = msg.fields() {
        if let Some(MessageField::Sender(sender)) = fields.get_field(MessageFieldCode::Sender) {
            return Some(sender.clone());
        }
    }
    None
}

/// from: https://github.com/bilelmoussaoui/ashpd/blob/master/src/desktop/screencast.rs (MIT)
#[derive(Debug, DeserializeDict, TypeDict)]
struct CreateSessionOptions {
    // handle_token: Option<String>,
    session_handle_token: Option<String>,
}

#[derive(Debug, DeserializeDict, TypeDict)]
struct CreateSessionResults {
    session_handle: Option<String>,
}
