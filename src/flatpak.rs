use tokio::process::Command;

#[derive(Debug, PartialEq)]
pub(crate) struct Process {
    pub(crate) app_id: String,
    pub(crate) pid: u32,
}

pub(crate) async fn ps() -> Vec<Process> {
    let mut cmd = Command::new("flatpak");
    cmd.arg("ps");
    cmd.arg("--columns=pid:f,application:f");

    let stdout = match cmd.output().await {
        Ok(o) => o.stdout,
        Err(_) => return vec![],
    };

    parse_ps_stdout(stdout)
}

fn parse_ps_stdout(stdout: Vec<u8>) -> Vec<Process> {
    let mut results = Vec::new();
    for line in String::from_utf8(stdout).unwrap_or_default().lines() {
        let parts: Vec<&str> = line
            .split_ascii_whitespace()
            .filter(|p| !p.is_empty())
            .collect();
        let pid = parts
            .first()
            .unwrap_or(&"")
            .parse::<u32>()
            .unwrap_or_default();
        let app_id = String::from(parts.last().unwrap_or(&"").to_owned());

        results.push(Process { app_id, pid });
    }
    results
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_ps_stdout_works() {
        let input = b"3000\torg.mozilla.firefox\n".to_vec();

        let got = parse_ps_stdout(input);

        assert_eq!(
            vec![Process {
                app_id: String::from("org.mozilla.firefox"),
                pid: 3000
            }],
            got
        );
    }
}
