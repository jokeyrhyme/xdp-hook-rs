use camino::Utf8PathBuf;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub(crate) struct CommandConfig {
    pub(crate) args: Option<Vec<String>>,
    pub(crate) cmd: Utf8PathBuf,
}

#[derive(Debug, Default, Deserialize, PartialEq)]
#[serde(default)]
pub(crate) struct Config {
    pub(crate) location: LocationConfig,
    pub(crate) remote_desktop: RemoteDesktopConfig,
    pub(crate) screen_cast: ScreenCastConfig,
}
impl From<String> for Config {
    fn from(input: String) -> Self {
        toml::from_str(&input).unwrap_or_default()
    }
}

#[derive(Debug, Default, Deserialize, PartialEq)]
#[serde(default)]
pub(crate) struct LocationConfig {
    pub(crate) all_sessions_closed: Option<Vec<CommandConfig>>,
    pub(crate) session_closed: Option<Vec<CommandConfig>>,
    pub(crate) session_created: Option<Vec<CommandConfig>>,
}

#[derive(Debug, Default, Deserialize, PartialEq)]
#[serde(default)]
pub(crate) struct RemoteDesktopConfig {
    pub(crate) all_sessions_closed: Option<Vec<CommandConfig>>,
    pub(crate) session_closed: Option<Vec<CommandConfig>>,
    pub(crate) session_created: Option<Vec<CommandConfig>>,
}

#[derive(Debug, Default, Deserialize, PartialEq)]
#[serde(default)]
pub(crate) struct ScreenCastConfig {
    pub(crate) all_sessions_closed: Option<Vec<CommandConfig>>,
    pub(crate) session_closed: Option<Vec<CommandConfig>>,
    pub(crate) session_created: Option<Vec<CommandConfig>>,
}

pub(crate) async fn read_config_file() -> String {
    if let Some(config_dir) = dirs::config_dir() {
        let config_path = config_dir.join(env!("CARGO_PKG_NAME")).join("config.toml");
        return tokio::fs::read_to_string(config_path)
            .await
            .unwrap_or_default();
    }
    String::new()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn config_from_string() {
        let input = String::from(
            r#"
[location]
session_closed = []

[[screen_cast.session_closed]]
cmd = "foo"

[[screen_cast.session_closed]]
args = []
cmd = "foo"

[[screen_cast.session_created]]
args = ["bar"]
cmd = "foo"
"#,
        );
        let want = Config {
            location: LocationConfig {
                all_sessions_closed: None,
                session_closed: Some(vec![]),
                session_created: None,
            },
            remote_desktop: RemoteDesktopConfig {
                all_sessions_closed: None,
                session_closed: None,
                session_created: None,
            },
            screen_cast: ScreenCastConfig {
                all_sessions_closed: None,
                session_closed: Some(vec![
                    CommandConfig {
                        args: None,
                        cmd: Utf8PathBuf::from("foo"),
                    },
                    CommandConfig {
                        args: Some(vec![]),
                        cmd: Utf8PathBuf::from("foo"),
                    },
                ]),
                session_created: Some(vec![CommandConfig {
                    args: Some(vec![String::from("bar")]),
                    cmd: Utf8PathBuf::from("foo"),
                }]),
            },
        };
        let got = Config::from(input);
        assert_eq!(want, got)
    }
}
