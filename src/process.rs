use std::path::PathBuf;

use tokio::fs;

use crate::flatpak;

#[derive(Debug, Default, PartialEq)]
pub(crate) struct Process {
    pub(crate) name: String,
    pub(crate) namespace_process_group_ids: Vec<u32>,
    pub(crate) namespace_session_ids: Vec<u32>,
    pub(crate) parent_pid: u32,
    pub(crate) pid: u32,
}

/// find an ancestor process that seems like an app,
/// e.g. when `pid` is a child process of a flatpak app,
/// return the name of that flatpak app command
pub(crate) async fn get_app(pid: u32) -> String {
    let p = get_process(pid).await;

    if Some(&p.pid) == p.namespace_process_group_ids.first()
        || Some(&p.pid) == p.namespace_session_ids.first()
    {
        // no container or pid namespace,
        // or owns it (if any)
        return String::new();
    }

    let apps = flatpak::ps().await;

    if apps.is_empty() {
        // no flatpak apps to match against
        return String::new();
    }

    for app in &apps {
        if app.pid == p.pid
            || Some(&app.pid) == p.namespace_process_group_ids.first()
            || Some(&app.pid) == p.namespace_session_ids.first()
        {
            return app.app_id.clone();
        }
    }

    // TODO: consider the non-container case where we need to check ancestor processes

    String::new()
}

/// retrieve/parse data from procfs: https://www.kernel.org/doc/html/latest/filesystems/proc.html
pub(crate) async fn get_process(pid: u32) -> Process {
    // short-circuit for process 0
    if pid == 0 {
        return Process::default();
    }

    let status_path = PathBuf::from(format!("/proc/{}/task/{}/status", pid, pid));
    let status_text = fs::read_to_string(status_path).await.unwrap_or_default();
    let mut name = "";
    let mut namespace_process_group_ids = vec![];
    let mut namespace_session_ids = vec![];
    let mut parent_pid = 0;
    for line in status_text.lines() {
        if line.starts_with("Name:") {
            name = line.split_ascii_whitespace().last().unwrap_or_default();
        } else if line.starts_with("PPid:") {
            let parent_pid_text = line.split_ascii_whitespace().last().unwrap_or_default();
            parent_pid = parent_pid_text.parse::<u32>().expect(
                "/proc/{pid}/task/{pid}/status contains unexpected string 'Ppid: {parent_pid}'",
            );
        } else if line.starts_with("NSpgid:") {
            namespace_process_group_ids = line
                .split_ascii_whitespace()
                .filter_map(|txt| txt.parse::<u32>().ok())
                .collect();
        } else if line.starts_with("NSsid:") {
            namespace_session_ids = line
                .split_ascii_whitespace()
                .filter_map(|txt| txt.parse::<u32>().ok())
                .collect();
        }
    }

    Process {
        name: String::from(name),
        namespace_process_group_ids,
        namespace_session_ids,
        parent_pid,
        pid,
    }
}

#[cfg(test)]
mod tests {
    use std::process;

    use super::*;

    #[tokio::test]
    async fn get_info_for_test_process() {
        let input = process::id();

        let got = get_process(input).await;

        // assumption: `cargo test` prepends executables' names like this
        assert!(got.name.starts_with("xdp_hook-"));

        assert!(got.parent_pid > 0);
        assert_eq!(input, got.pid);
    }
}
