use std::io;

use camino::Utf8PathBuf;
use tokio::process::Command;

use crate::{
    config::{CommandConfig, Config},
    portal::Interface,
    process::{get_app, get_process, Process},
    sessions::Session,
};

pub(crate) async fn all_sessions_closed(interface: &Interface, cfg: &Config) {
    for hook in all_sessions_closed_hooks(interface, cfg) {
        let cmd = prepare_command(hook, None, None);
        run_command(cmd).await;
    }
}

pub(crate) async fn session_closed(session: &Session, cfg: &Config) {
    let p = get_process(session.process_id).await;
    let a = get_app(session.process_id).await;
    for hook in session_closed_hooks(&session.interface, cfg) {
        let cmd = prepare_command(hook, Some(&p), Some(&a));
        run_command(cmd).await;
    }
}

pub(crate) async fn session_created(session: &Session, cfg: &Config) {
    let p = get_process(session.process_id).await;
    let a = get_app(session.process_id).await;
    for hook in session_created_hooks(&session.interface, cfg) {
        let cmd = prepare_command(hook, Some(&p), Some(&a));
        run_command(cmd).await;
    }
}

const TILDE_PREFIX: &str = "~/";

fn expand_tilde_home(path: Utf8PathBuf) -> Utf8PathBuf {
    if !path.starts_with(TILDE_PREFIX) {
        return path;
    }
    let home_dir = match dirs::home_dir() {
        Some(h) => match Utf8PathBuf::from_path_buf(h) {
            Ok(utf8_h) => utf8_h,
            Err(e) => {
                eprintln!(
                    "{}: cannot expand {:?}: only supports UTF-8 paths: {:?}",
                    env!("CARGO_PKG_NAME"),
                    &path,
                    e,
                );
                return path;
            }
        },
        None => {
            eprintln!(
                "{}: cannot expand {:?}: unable to determine home directory",
                env!("CARGO_PKG_NAME"),
                &path,
            );
            return path;
        }
    };
    match path.strip_prefix(TILDE_PREFIX) {
        Ok(tail) => home_dir.join(tail),
        Err(e) => {
            // this ought to be unreachable because of the `.starts_with()` check,
            // but just in case ...
            eprintln!(
                "{}: cannot expand {:?}: unexpected prefix: {:?}",
                env!("CARGO_PKG_NAME"),
                &path,
                e,
            );
            path
        }
    }
}

fn log_command(cmd: &Command, err: Option<io::Error>, exit_code: Option<i32>) {
    if let Some(e) = err {
        eprintln!("{}: {:?} -> {:?}", env!("CARGO_PKG_NAME"), cmd.as_std(), e);
        return;
    }
    match exit_code {
        Some(code) => eprintln!(
            "{}: {:?} -> exit code {}",
            env!("CARGO_PKG_NAME"),
            cmd.as_std(),
            code
        ),
        None => eprintln!("{}: {:?} -> killed", env!("CARGO_PKG_NAME"), cmd.as_std()),
    }
}

fn prepare_command(
    command: CommandConfig,
    process_info: Option<&Process>,
    app: Option<&str>,
) -> Command {
    let exe = expand_tilde_home(command.cmd.clone());
    let mut cmd = Command::new(exe);
    if let Some(args) = command.args {
        for arg in args {
            cmd.arg(arg);
        }
    }
    if let (Some(p), Some(a)) = (process_info, app) {
        cmd.env("XDP_HOOK_APP_ID", &a);
        cmd.env("XDP_HOOK_PROCESS_COMMAND", &p.name);
        cmd.env("XDP_HOOK_PROCESS_ID", format!("{}", p.pid));
    }
    cmd
}

async fn run_command(mut cmd: Command) {
    let mut child = match cmd.spawn() {
        Ok(c) => c,
        Err(e) => {
            log_command(&cmd, Some(e), None);
            return;
        }
    };
    match child.wait().await {
        Ok(s) => log_command(&cmd, None, s.code()),
        Err(e) => {
            log_command(&cmd, Some(e), None);
            return;
        }
    }
    if let Err(e) = child.wait().await {
        log_command(&cmd, Some(e), None);
    }
}

fn all_sessions_closed_hooks(interface: &Interface, cfg: &Config) -> Vec<CommandConfig> {
    let maybe_hooks = match interface {
        Interface::Location => &cfg.location.all_sessions_closed,
        Interface::ScreenCast => &cfg.screen_cast.all_sessions_closed,
        Interface::RemoteDesktop => &cfg.remote_desktop.all_sessions_closed,
    };
    maybe_hooks.as_ref().cloned().unwrap_or_default()
}

fn session_closed_hooks(interface: &Interface, cfg: &Config) -> Vec<CommandConfig> {
    let maybe_hooks = match interface {
        Interface::Location => &cfg.location.session_closed,
        Interface::ScreenCast => &cfg.screen_cast.session_closed,
        Interface::RemoteDesktop => &cfg.remote_desktop.session_closed,
    };
    maybe_hooks.as_ref().cloned().unwrap_or_default()
}

fn session_created_hooks(interface: &Interface, cfg: &Config) -> Vec<CommandConfig> {
    let maybe_hooks = match interface {
        Interface::Location => &cfg.location.session_created,
        Interface::ScreenCast => &cfg.screen_cast.session_created,
        Interface::RemoteDesktop => &cfg.remote_desktop.session_created,
    };
    maybe_hooks.as_ref().cloned().unwrap_or_default()
}

#[cfg(test)]
mod tests {
    use std::ffi::OsStr;

    use camino::Utf8PathBuf;

    use super::*;

    #[test]
    fn prepare_command_with_args() {
        let cfg = CommandConfig {
            args: Some(vec![String::from("first"), String::from("second")]),
            cmd: Utf8PathBuf::from("foo"),
        };

        let cmd = prepare_command(cfg, None, None);
        let got = cmd.as_std();

        assert_eq!("foo", got.get_program());
        assert_eq!(2, got.get_args().len());
        assert_eq!(
            vec!["first", "second"],
            got.get_args().collect::<Vec<&OsStr>>()
        );
    }

    #[test]
    fn prepare_command_with_info() {
        let cfg = CommandConfig {
            args: None,
            cmd: Utf8PathBuf::from("foo"),
        };

        let process_info = Process {
            name: String::from("cargo"),
            parent_pid: 122,
            pid: 123,
            ..Default::default()
        };

        let cmd = prepare_command(cfg, Some(&process_info), Some("blarg"));
        let got = cmd.as_std();

        assert_eq!("foo", got.get_program());
        assert_eq!(0, got.get_args().len());
        assert_eq!(
            vec![
                (OsStr::new("XDP_HOOK_APP_ID"), Some(OsStr::new("blarg"))),
                (
                    OsStr::new("XDP_HOOK_PROCESS_COMMAND"),
                    Some(OsStr::new("cargo"))
                ),
                (
                    OsStr::new("XDP_HOOK_PROCESS_ID"),
                    Some(OsStr::new(&format!("{}", 123)))
                ),
            ],
            got.get_envs().collect::<Vec<(&OsStr, Option<&OsStr>)>>()
        );
    }

    #[test]
    fn prepare_command_with_no_args() {
        let cfg = CommandConfig {
            args: None,
            cmd: Utf8PathBuf::from("foo"),
        };

        let cmd = prepare_command(cfg, None, None);
        let got = cmd.as_std();

        assert_eq!("foo", got.get_program());
        assert_eq!(0, got.get_args().len());
    }

    #[test]
    fn prepare_command_with_tilde() {
        let cfg = CommandConfig {
            args: None,
            cmd: Utf8PathBuf::from("~/.local/bin/foo"),
        };

        let cmd = prepare_command(cfg, None, None);
        let got = cmd.as_std();

        assert_eq!(
            format!("{}/.local/bin/foo", env!("HOME")),
            got.get_program().to_string_lossy()
        );
        assert_eq!(0, got.get_args().len());
    }
}
